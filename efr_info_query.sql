SELECT c.company_name,
       ef.field_id,
       c.fmid AS company_fmid,
       fa.farm_name,
       f.field_name,

       e.efr_field_id,
       e.efr_id,
       e.crop,
       e.activity,
       e.certified_ts,
       e.start_date,
       e.end_date,

        st_asgeojson(st_centroid(b.boundary)) as centroid
FROM electronic_field_record.efr AS e
       INNER JOIN electronic_field_record.efr_field AS ef
                  ON e.efr_field_id = ef.efr_field_id


       INNER JOIN electronic_field_record.field AS f
                  ON ef.field_id = f.field_id
       INNER JOIN electronic_field_record.farm AS fa
                  ON fa.farm_id = f.farm_id
       INNER JOIN account.company AS c
                  ON c.company_id = fa.company_id
       INNER JOIN electronic_field_record.boundary as b
                  ON b.field_id = f.field_id

WHERE e.start_date >= '2018-01-01'
  AND e.activity = 'plant'
  AND e.crop = 'Corn'
  AND e.efr_field_id = 0000000